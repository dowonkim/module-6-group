from django.conf.urls import patterns, include, url
from calender.api import UserResource, subCalResource, EventResource

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

user_resource = UserResource()
subcal_resource = subCalResource()
event_resource = EventResource()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'myCalender.views.home', name='home'),
    # url(r'^myCalender/', include('myCalender.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # Templates
    url(r'^', include('calender.urls')),
 
    # RESTful URLs
    url(r'^', include(user_resource.urls)),
    url(r'^', include(subcal_resource.urls)),
    url(r'^', include(event_resource.urls)),
)
