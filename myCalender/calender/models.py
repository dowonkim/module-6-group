from django.db import models
from django.contrib.auth.models import User

# Users own specific subCalenders
class subCal(models.Model):
	user = models.ForeignKey(User)
	#name of subcal
	name = models.CharField(max_length = 20)
	#integer to store color 
	color = models.IntegerField(default = 1)
	#User who owns this calender
	def __unicode__(self):
		return self.name

#subCalenders own Events
class Event(models.Model):
	#The only foreign key is subcalender, you don't actually need to have events be associated with users
	#since subcalenders are associated with users. A user only gets the events for his current subcalender.
	subCal = models.ForeignKey(subCal)
	#the name is one of the only 2 editable parts
	title = models.CharField(max_length = 30)
	#you can also edit the start and end time
	start = models.DateTimeField()
	def __unicode__(self):
		return self.title
