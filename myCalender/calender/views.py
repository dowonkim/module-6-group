# Create your views here.
from datetime import datetime
from django.shortcuts import render
from calender.models import Event, subCal
from calender.api import EventResource
from annoying.decorators import ajax_request #Annoying is a cool little package to help django be less annoying
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User, UserManager
from django.views.decorators.http import require_http_methods
from django.views.decorators.http import HttpResponse
from tastypie.resources import ModelResource

def index(request):
    return render(request, 'calender/index.html', {})
    
@ajax_request
@require_http_methods(["POST"])
def login_reg(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return {"success" : "true", "username" : username, "user_id": user.id}
    else:
        return {"success" : "false", "reason" : "Invalid password"}

@ajax_request
@require_http_methods(["POST"])
def register(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is None and User.objects.filter(username = username).count() == 0:
        user = User.objects.create_user(username, "", password)
        return {"success" : "true", "username" : username, "user_id" : user.id}
    else:
        return {"status" : "false", "reason" : "Invalid password"}
    
@ajax_request
@require_http_methods(["POST"])
def addCal(request):
    calName = request.POST['calName']
    color = request.POST['color']
    if request.user.is_authenticated():
        if len(calName) < 20 and len(calName) > 0:
            user = request.user
            subcalender = subCal(color = color, name = calName, user = request.user)
            subcalender.save()
            return {"success" : "added", "subCal": calName}
        else:
            return {"status" : "false", "reason": "invalid calender name"}
    else:
        return{"status" : "false", "reason": "not logged in"}
    
@ajax_request
@require_http_methods(["POST"])
def delCal(request):
    calid = request.POST['calid']
    user = request.user
    if request.user.is_authenticated():
        if subCal.objects.filter(id = calid).filter(user = request.user).count() > 0:
            subCal.objects.filter(id = calid).delete();
            return{"success" : "true"}
        else:
            return{"status" : "false", "reason": "bad request"}
    else:
        return{"status" : "false", "reason": "not logged in"}
    
@ajax_request
@require_http_methods(["POST"])
def addEvent(request):
    calid = int(request.POST['calid'])
    sub = subCal.objects.get(pk=calid)
    title = request.POST['title']
    year = int(request.POST['year'])
    month = int(request.POST['month'])
    day = int(request.POST['day'])
    hour = int(request.POST['hour'])
    minute = int(request.POST['minute'])
    if request.user.is_authenticated():
        if sub.user == request.user:
            datepower = datetime(year,month,day,hour,minute)
            newEvent = Event(subCal = sub, title = title, start = datepower)
            newEvent.save();
            return{"success" : "true"}
        else:
            return{"status":"false", "reason" : "stop being a hacker"}
    else:
        return{"status":"false", "reason": "not logged in"}
    
    
@ajax_request
@require_http_methods(["POST"])
def delEvent(request):
    user = request.user
    calid = int(request.POST['calid'])
    eid = int(request.POST['eventid'])
    sub = subCal.objects.get(pk=calid)
    event = Event.objects.get(pk = eid)
    if event == None:
        return {"status" : "false", "reason" : "invalid event id"}
    if event.subCal != sub:
        return {"status": "false", "reason" : "event and calender don't match"}
    if request.user.is_authenticated():
        if sub.user == request.user:
            event.delete()
            return{"success" : "true"}
        else:
            return{"status":"false", "reason" : "stop being a hacker"}
    else:
        return{"status":"false", "reason": "not logged in"}
    
@ajax_request
@require_http_methods(["POST"])
def editEvent(request):
    eid = int(request.POST['eventid'])
    calid = int(request.POST['calid'])
    sub = subCal.objects.get(pk=calid)
    event = Event.objects.get(pk = eid)
    if event == None:
        return {"status" : "false", "reason" : "invalid event id"}
    if event.subCal != sub:
        return {"status": "false", "reason" : "event and calender don't match"}
    title = request.POST['title']
    year = int(request.POST['year'])
    month = int(request.POST['month'])
    day = int(request.POST['day'])
    hour = int(request.POST['hour'])
    minute = int(request.POST['minute'])
    if request.user.is_authenticated():
        if sub.user == request.user:
            datepower = datetime(year,month,day,hour,minute)
            event.start = datepower
            event.title = title
            event.save();
            return{"success" : "true"}
        else:
            return{"status":"false", "reason" : "stop being a hacker"}
    else:
        return{"status":"false", "reason": "not logged in"}