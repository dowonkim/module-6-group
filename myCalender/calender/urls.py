from django.conf.urls import patterns, url
from calender import views, getCalenders
 
urlpatterns = patterns('',
    url(r'^login_auth.py$', views.login_reg, name = 'login_reg'), #working
    url(r'^register.py$', views.register, name = 'register'), #about to test
    #url(r'^getCalenders.py$', getCalenders.subCalList(), name = 'get_list'), #functioning via tastypie
    url(r'^addCal.py$', views.addCal, name = 'addCal'), #about to test
    url(r'^delCal.py$', views.delCal, name = 'delCal'), #could function via tastypie, let's not though
    #get events by calender/month is handled by tastypie. I wonder if this works.
    #url(r'^getEvents.py$', views.getEvents, name = 'getEvents'),
    #forget this, I'm using GET to get events. it makes sense to use GET to GET something
    url(r'^addEvent.py$', views.addEvent, name = 'addEvent'), #will test after addCal, hardest part
    url(r'^delEvent.py$', views.delEvent, name = 'delEvent'), #will test after delCal.
    url(r'^editEvent.py$', views.editEvent, name = 'editEvent'), #will test after delEvent.
    url(r'^$', views.index, name='index') #working
)
