from tastypie.authorization import Authorization
from tastypie.resources import ModelResource
from calender.models import subCal
from tastypie.paginator import Paginator
from calender.api import UserResource
from tastypie.fields import ToOneField
import tastypie


class subCalList(ModelResource):
	user = tastypie.fields.ForeignKey(UserResource, 'user')
	class Meta:
		authorization = Authorization()
		queryset = subCal.objects.all()
		paginator_class = Paginator
	def get_list(self, bundle, **kwargs):
		user = bundle.request.user
		user_id = bundle.request.POST['id']
		if user_id == user.id:
			return queryset.filter(user__id = user_id)	
		else:
			return queryset.filter(user__id = -1)
