from annoying.decorators import ajax_request
from calender.models import subCal, Event
from django.contrib.auth.models import User
from django.utils import simplejson
from django.core.serializers import json
from django.views.decorators.http import require_http_methods
from tastypie.resources import ModelResource
from tastypie.paginator import Paginator
from tastypie.fields import ToOneField
from tastypie.serializers import Serializer
import tastypie
 
class UserResource(ModelResource):
	class Meta:
		queryset = User.objects.all()
		resource_name = 'user'
		fields = ['username', 'id']
		paginator_class = Paginator

#Debugging this stuff class was a horrifying experience, I would NOT reccomend it.
class subCalResource(ModelResource):
	user = tastypie.fields.ForeignKey(UserResource, 'user')
	class Meta:
		queryset = subCal.objects.all()
		paginator_class = Paginator
	def get_object_list(self, request):
		if request.user:
			return super(subCalResource, self).get_object_list(request).filter(user__id = request.user.id)
		return super(subCalResource, self).get_object_list(request)

class EventResource(ModelResource):
	cid = ToOneField(subCalResource, "subCal", full=True)
	class Meta:
		queryset = Event.objects.all()
		paginator_class = Paginator
		always_return_data = True
	def dehydrate_cid(self, bundle):
		return bundle.obj.subCal.id
	def get_object_list(self, request):
		calid = request.GET.get('calid', None)
		if calid:
			#check if user owns the calender
			sub = subCal.objects.get(pk=calid)
			if sub.user == request.user:
				return super(EventResource, self).get_object_list(request).filter(subCal__id = calid)
		#if nobody is making a get request, then this is treated as someone visiting the url. Show nothing.
		return super(EventResource, self).get_object_list(request).filter(subCal__id = -1)
	




